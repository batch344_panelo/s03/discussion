package com.zuitt.example;

import java.util.Arrays;

public class RepetitionControl {
    public static void main(String[] args){
        //[SECTION] Loops
        //are control Structures that allow code blocks to be executed multiple times

        //While Loop
        //allow for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate is indefinite

        //to create the variable that will serve as the basis of our condition

        //sa while loop need mo muna magdeclare ng variable, otherwase hindi mo magagamit
        //need maglagay ng increment or decrement kung hindi, hindi magsstop yung while loop
        // ++x (pre-increment); x++ (post-increment)

        /*int x = 0;
        while(x < 10){
            x++;
            System.out.println("The current value of x is: " + x);
          }
         */

        //Do-While Loop
        //similar to while loop. However, do-while loops always execute at least once - while loops may not execute/rust at all

        int y = 11;
        do{
            System.out.println("The current value of y is: " + y);
            y++;
        }while(y < 10);

        //For-loop:
            //SYNTAX:
                //for(initialVal; condition/stopper; iteration{codeblock/statement});

        for(int i = 0; i < 10; i++) {
            System.out.println("The current value of i is: " + i);
        }

        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        //for loops for Arrays:
        for(String name : nameArray) {
            System.out.println(name);
        }

        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //for-loop in a multidimensional array
        for(String[] row : classroom){
            //add another for loop that will iterate on the nested array inside our classroom array
            //System.out.println(Arrays.toString(row));
            for(String column : row){
                System.out.println(column);
            }
        }






    }
}
